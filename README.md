# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app)

## Available Scripts

In the project directory, you can run:

### `npm start`

**Before you run, please generate your github developer token and add in src/api/config.js**

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### External Libraries:
- [React Pagination](https://www.npmjs.com/package/react-paginate): For pagination.
- [Boostrapp](https://getbootstrap.com/): For pagination css. 

### TODO:
- Test with Jest
- Remove deadcode. 
