import axios from "axios"
import { endPoints } from "./config"


export async function queryData(query_type, value, opt, page) {
    const url = opt === "users" ? `${endPoints.git_url}search/${query_type || opt}?q=${value}&per_page=${endPoints.per_page}&page=${page}` : `${endPoints.git_url}orgs/${value}`
    
    return await axios.get(url, {headers: {Authorization: `token ${endPoints.token}`}}).then(
        (response) => { return response.data }
    ).catch(error => {
        throw error
    })
}