import "./home.scss"
import { Start } from "../../components/home/start"
import { useSelector } from 'react-redux'
import { Result } from "../../container/result/result"
import { useParams } from "react-router";
import { useLocation } from 'react-router-dom';
import queryString from 'query-string'

export const Home = () => {

    let { opt } = useParams();
    const { search } = useLocation()
    const values = queryString.parse(search)

    const printResult = () => {
        if (opt || values.name) {
            return <Result></Result>
        }
    }

    return (
        <div className="home-container">
            <div className="home-inner-container">
                <Start query_type={opt} />
                { printResult() }
            </div>
        </div>
    )
}