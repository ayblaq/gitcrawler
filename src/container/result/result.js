import "./result.scss"
import { useEffect, useState } from "react"
import { Empty } from "../../components/empty/empty"
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from "react-router";
import { useLocation } from 'react-router-dom';
import queryString from 'query-string'
import { queryData } from "../../api/api"
import { Loader } from "../../components/loader/loader";
import { error_block, result_title } from "../../content/content";
import { Paginated } from "../../components/pagination/pagination";
import { optionSlice, setPage } from "../../appRedux/slices/option/option";

export const Result = (props) => {

    const [loading, setLoading] = useState(props)
    const [data, setData] = useState({ result: [], error: "" })
    const option = useSelector((state) => state.option)
    let { opt } = useParams();
    const { search } = useLocation()
    const values = queryString.parse(search)
    const type = values.name;


    useEffect(() => {
        fetchData()
    }, [opt, values.name, option.page])

    const dispatch = useDispatch()
    const fetchPerPage = (page_number) => {
        dispatch(setPage(page_number))
    }

    const fetchData = () => {
        setLoading(true)
        setTimeout(() => {
            queryData(option.title, type, opt, option.page).then((data) => {
                setData({ result: data, error: "" })
                setLoading(false)
            }).catch((error) => {
                setData({ result: [], error: error_block.subtitle })
                setLoading(false)
            })
        }, 1000)
    }

    if (loading) {
        return <Loader />
    } else if (data.error != "") {
        return (
            <div className="result-container">
                <div className="result-inner-container">
                    <Empty
                        title={error_block.title}
                        subtitle={data.error}
                    />
                </div>
            </div>
        )
    } else {
        return (
            <div>
                <div className="result-container">
                    <div className="result-inner-container">
                        <p className="title">{result_title} {type} {data.result.login ? "" : `Total (${data.result.items.length})`}</p>
                        <div className="result-content">
                            {
                                data.result.login ? <Singleresult
                                    avatar={data.result.avatar_url}
                                    name={data.result.name}
                                    description={data.result.description}
                                    location={data.result.location}
                                />
                                    :
                                    data.result.items.map((item, index) => (
                                        <Singleresult
                                            key={index}
                                            avatar={item.avatar_url}
                                            login={item.login}
                                            url={item.html_url}
                                        />
                                    ))
                            }
                        </div>
                    </div>
                </div>
                {option.title === "users" ? <Paginated total_count={data.result.total_count} clickFunc={fetchPerPage} cur_item={option.page-1}></Paginated> : <></>}
            </div>
        )
    }
}

const Singleresult = (props) => {
    return (
        <div className="singleresult-container">
            <div className="singleresult-inner-container">
                <img src={props.avatar} alt="Option avi" />
                <div className="content">
                    <p>{props.name || props.login}</p>
                    <p>{props.description || <a href={props.url} className={"user_link"} target="_blank"><i className="fas fa-link">see profile</i></a>}</p>
                    {props.location && <p>{props.location}</p>}
                </div>
            </div>
        </div>
    )
}