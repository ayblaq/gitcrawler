import { createSlice } from '@reduxjs/toolkit'

export const optionSlice = createSlice({
  name: 'option',
  initialState: {
    title: 'users',
    page: 1,
  },
  reducers: {
    setOption: (state, action) => {
      return {
        ...state, title: action.payload, page: 1
      }
    },
    setPage: (state, action) => {
      return {
        ...state, page: action.payload
      }
    },
    close: (state) => {
      return {
        ...state, page: 1, title: false
      }
    },
  },
})

export const { setOption, close, setPage } = optionSlice.actions

export default optionSlice.reducer