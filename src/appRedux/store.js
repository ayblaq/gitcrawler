import { configureStore } from '@reduxjs/toolkit'
import  optiontReducer  from './slices/option/option'
import  searchReducer  from './slices/search/search'
export default configureStore({
  reducer: {
      option: optiontReducer,
      search: searchReducer
  },
})