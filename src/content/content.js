export const toggledata = [
    {
        title: "Organisations",
        name: 'organizations',
    },
    {
        title: "Users",
        name: 'users',
    }
]

export const error_block = {
    title: "Oops :(",
    subtitle: "Nothing to show, Search results came out empty."
}

export const result_title = "Showing Results For";