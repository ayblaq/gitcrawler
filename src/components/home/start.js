import "./start.scss"
import { Searchbox } from "../search/searchbox"
import { useSelector, useDispatch } from 'react-redux'
import { setOption } from "../../appRedux/slices/option/option"
import { ToggleBox } from "../togglebox/togglebox"
import { toggledata } from "../../content/content"
import { useEffect } from "react"

export const Start = (props) => {

    const {query_type } = props

    const data = toggledata

    const dispatch = useDispatch()

    const option = useSelector((state) => state.option)

    useEffect(() => {
        if (query_type != option.title && query_type) {
            dispatch(setOption(query_type))
        }
    }, [])

    var setChoice = (item) => {
        dispatch(setOption(item));
    }

    return <div className="start-input">
        <ToggleBox data={data} option={option} clickFunc={setChoice}></ToggleBox>
        <Searchbox />
    </div>
}