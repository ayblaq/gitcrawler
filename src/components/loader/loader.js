import "./loader.scss"

export const Loader = () => {
    return (
        <div className="loader-container">
                <div className="loader-inner-container">
                    <div className="loader"></div>
                    Loading.....
                </div>
            </div>
    )
}