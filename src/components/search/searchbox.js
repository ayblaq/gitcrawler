import "./searchbox.scss"
import { Singleinput } from "../inputs/singleinput"
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux'
import { setValue } from "../../appRedux/slices/search/search";

export const Searchbox = (props) => {

    const search = useSelector((state) => state.search)
    const option = useSelector((state) => state.option)
    const history = useHistory();
    const dispatch = useDispatch()

    const handleChange = (e) => {
        var value = e.target.value
        dispatch(setValue(value));
    }

    var navigate = (value) => {
        history.push(value)
    }

    return (
        <div className="searchbox-container">
            <Singleinput
                type="text"
                label=""
                name="single-box"
                placeholder= {`Enter name of ${option.title || props.title}`}
                value={search.value}
                disabled={false}
                onChange={handleChange}
            />
            <button onClick={() => navigate(`/${option.title}?name=${search.value}`)} disabled={search.value === '' ? true : false}> SUBMIT </button>
        </div>
    )
}