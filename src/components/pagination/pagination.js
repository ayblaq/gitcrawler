import "./pagination.scss"
import { useEffect, useState } from "react"
import ReactPaginate from 'react-paginate';
import { endPoints } from "../../api/config";


export const Paginated = (props) => {
    const { total_count, clickFunc, cur_item } = props

    const items = [...Array(total_count).keys()].map(i => i + 1);
    const pagination_per_page = endPoints.per_page

    const [currentItems, setCurrentItems] = useState(null);
    const [pageCount, setPageCount] = useState(0);
    const [itemOffset, setItemOffset] = useState(0);

    useEffect(() => {
        const endOffset = itemOffset + pagination_per_page;
        setCurrentItems(items.slice(itemOffset, endOffset));
        setPageCount(Math.ceil(items.length / pagination_per_page));
    }, [itemOffset, cur_item]);

    const handlePageClick = (event) => {
        const newOffset = (event.selected * pagination_per_page) % items.length;
        setItemOffset(newOffset);
        clickFunc(event.selected+1)
    };


    return (
        <div>
            <Items selections={currentItems} />
            <ReactPaginate
                nextLabel="next >"
                onPageChange={handlePageClick}
                pageRangeDisplayed={4}
                marginPagesDisplayed={2}
                forcePage={cur_item}
                pageCount={pageCount}
                previousLabel="< previous"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakLabel="..."
                breakClassName="page-item"
                breakLinkClassName="page-link"
                containerClassName="pagination"
                activeClassName="active"
                renderOnZeroPageCount={null}
            />
        </div>
    );
}

export const Items = (props) => {
    const { selections } = props

    return (
        <>
            {selections &&
                selections.map((item, key) => (
                    <div key={key}>
                    </div>
                ))}
        </>
    )
}