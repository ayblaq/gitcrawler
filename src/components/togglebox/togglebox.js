import "./togglebox.scss"


export const ToggleBox = (props) => {
    const { data, option, clickFunc } = props

    return (
        <div className="toggle-container">
            <div className="toggle-inner-container">
                <div className="toggle">
                    {
                        data.map((item, index) => (
                            <span key={index}>
                                <input type='radio' value={option.title} name={`radio-${index}`} id={`radio-${index}`} onChange={e => {}} checked={option.title === item.name ? true : false} />
                                <label className={option.title === item.name ? "active" : ""} htmlFor={`radio-${index}`} onClick={() => clickFunc(item.name)}>{item.title}</label>
                            </span>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}