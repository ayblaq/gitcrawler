import './App.scss';
import { Switch, Route } from 'react-router';
import { Home } from '../container/home/home';
import { Result } from '../container/result/result';
function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/:opt" component={Home} />
        <Route component={Error} />
      </Switch>
    </div>
  );
}

export default App;
